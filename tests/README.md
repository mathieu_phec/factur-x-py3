# Testing with Virtual Env

* On root directory
* Run your virtualenv `virtualenv env --no-site-packages`
* Activate your virtualenv `source env/bin/activate`
* Install pip requierements `pip install -r requirement.txt --upgrade`
* Run pytest `python -m pytest -v tests`
* Deactivate your virtualenv `deactivate`