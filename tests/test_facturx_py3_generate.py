import facturx
from pathlib import Path

def test_generate_facturx_from_file():
    """
        Genere un fichier PDF avec des metadatas FacturX
    """
    pdf_filepath = "tests/facture_test_minimum.pdf"
    xml_filepath = "tests/facture_test_minimum.xml"
    #We need to load xml file and not pass xml_filepath
    with open(xml_filepath, "rb") as f:
        f.seek(0)
        xml_data = f.read()
    facturx_pdf_filepath = "tests/facture_test_minimum_facturx.pdf"

    # On supprime le fichier de destination
    if Path(facturx_pdf_filepath).is_file():
        Path(facturx_pdf_filepath).unlink()

    assert facturx.generate_facturx_from_file(
        pdf_invoice=pdf_filepath,
        facturx_xml=xml_data,
        output_pdf_file=facturx_pdf_filepath
    ) == True

    assert Path(facturx_pdf_filepath).is_file()

def test_generate_facturx_from_binary():
    """
        Genere un fichier PDF avec des metadatas FacturX a partir de données
        binaire
    """

    pdf_filepath = "tests/facture_test_minimum.pdf"
    xml_filepath = "tests/facture_test_minimum.xml"
    facturx_pdf_filepath = "tests/facture_test_minimum_facturx_2.pdf"


    with open(pdf_filepath, "rb") as f:
        f.seek(0)
        pdf_data = f.read()

    with open(xml_filepath, "rb") as f:
        f.seek(0)
        xml_data = f.read()

    # On supprime le fichier de destination
    if Path(facturx_pdf_filepath).is_file():
        Path(facturx_pdf_filepath).unlink()

    with open(facturx_pdf_filepath, "wb") as f:
        facturx_data = facturx.generate_facturx_from_binary(
            pdf_invoice=pdf_data,
            facturx_xml=xml_data
        )
        f.write(facturx_data)

    assert Path(facturx_pdf_filepath).is_file()