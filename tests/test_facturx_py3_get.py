import facturx

def test_get_facturx_level():
    """
        Vérifie le niveau facturx d'un xml
    """
    from lxml import etree
    with open("tests/facture_test_minimum.xml", "rb") as f:
        f.seek(0)
        xml_string = f.read()
        facturx_xml_etree = etree.fromstring(xml_string)
        facturx_level = facturx.get_facturx_level(facturx_xml_etree)
        assert facturx_level == "minimum"

def test_get_facturx_flavor():
    """
        Vérifie que le xml est bien pour factur-x (flavor)
    """
    from lxml import etree
    with open("tests/facture_test_minimum.xml", "rb") as f:
        f.seek(0)
        xml_string = f.read()
        facturx_xml_etree = etree.fromstring(xml_string)
        facturx_flavor = facturx.get_facturx_flavor(facturx_xml_etree)
        assert facturx_flavor == "factur-x"