import facturx

def test_check_xml_from_pdf():
    """
        Vérifie le XML dans un fichier PDF
    """
    with open("tests/Facture_FR_MINIMUM.pdf", "rb") as f:
        xml_name, xml_data = facturx.get_facturx_xml_from_pdf(f)
        assert xml_name == "factur-x.xml"
        assert xml_data != None

def test_check_facturx_xsd():
    """
        Vérifie la conformité d'un XML Facturx
    """
    with open("tests/facture_test_minimum.xml", "rb") as f:
        assert facturx.check_facturx_xsd(f) == True
